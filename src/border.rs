use bevy::prelude::*;

use crate::{
    block::{Block, BlockSize},
    field::{FIELD_HEIGHT, FIELD_WIDTH},
    position::Position,
};

#[derive(Debug)]
pub(crate) struct Border;

pub(crate) fn setup_border(
    commands: &mut Commands,
    block_size: Res<BlockSize>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    let color = Color::hex("6d214f").unwrap();
    let material = materials.add(color.into());
    for &y in [-1, FIELD_HEIGHT].iter() {
        for x in -1..FIELD_WIDTH + 1 {
            Block::spawn(
                commands,
                material.clone(),
                block_size.0,
                Position { x, y },
            )
            .with(Border);
        }
    }
    for &x in [-1, FIELD_WIDTH].iter() {
        for y in 0..FIELD_HEIGHT {
            Block::spawn(
                commands,
                material.clone(),
                block_size.0,
                Position { x, y },
            )
            .with(Border);
        }
    }
}
