use bevy::prelude::*;
use rand::prelude::*;

use crate::{
    block::{Block, BlockSize},
    field::{Field, FieldEvent, FIELD_HEIGHT, FIELD_WIDTH},
    position::Position,
};

#[derive(Clone, Debug, Default)]
pub(crate) struct Piece {
    blocks: Vec<Position>,
    size: i32,
}

/// Position of piece on playing field. (0, 0) is upper left and x increases to
/// the right and y increases downwards.
#[derive(Clone, Copy, Debug)]
pub(crate) struct PiecePosition(pub Position);

/// A component to indicate that some piece being movable (by a gamepad for
/// example). Keeps track of the piece's size.
#[derive(Debug)]
pub(crate) struct ActivePiece {
    pub piece_size: i32,
}

#[derive(Debug)]
pub(crate) struct GhostPiece;

#[derive(Debug)]
pub(crate) struct GhostBlock;

#[derive(Clone, Debug)]
struct Materials {
    middle_blue: Handle<ColorMaterial>,
    blurple: Handle<ColorMaterial>,
    quince_jelly: Handle<ColorMaterial>,
    sun_flower: Handle<ColorMaterial>,
    seabrook: Handle<ColorMaterial>,
    protoss_pylon: Handle<ColorMaterial>,
    light_greenish_blue: Handle<ColorMaterial>,
    exodus_fruit: Handle<ColorMaterial>,
    pico_8_pink: Handle<ColorMaterial>,
    chi_gong: Handle<ColorMaterial>,
}

#[derive(Debug)]
pub(crate) enum PieceEvent {
    Spawn,
    Lock,
}

pub(crate) fn create_pieces(
    commands: &mut Commands,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    let middle_blue = Color::hex("7ed6df").unwrap();
    let blurple = Color::hex("4834d4").unwrap();
    let quince_jelly = Color::hex("f0932b").unwrap();
    let sun_flower = Color::hex("f1c40f").unwrap();
    let seabrook = Color::hex("487eb0").unwrap();
    let protoss_pylon = Color::hex("00a8ff").unwrap();
    let light_greenish_blue = Color::hex("55efc4").unwrap();
    let exodus_fruit = Color::hex("6c5ce7").unwrap();
    let pico_8_pink = Color::hex("fd79a8").unwrap();
    let chi_gong = Color::hex("d63031").unwrap();

    let materials = Materials {
        middle_blue: materials.add(middle_blue.into()),
        blurple: materials.add(blurple.into()),
        quince_jelly: materials.add(quince_jelly.into()),
        sun_flower: materials.add(sun_flower.into()),
        seabrook: materials.add(seabrook.into()),
        protoss_pylon: materials.add(protoss_pylon.into()),
        light_greenish_blue: materials.add(light_greenish_blue.into()),
        exodus_fruit: materials.add(exodus_fruit.into()),
        pico_8_pink: materials.add(pico_8_pink.into()),
        chi_gong: materials.add(chi_gong.into()),
    };

    commands.insert_resource(materials.clone());

    let i = Piece {
        blocks: vec![
            Position::new(0, 1),
            Position::new(1, 1),
            Position::new(2, 1),
            Position::new(3, 1),
        ],
        size: 4,
    };
    let j = Piece {
        blocks: vec![
            Position::new(0, 0),
            Position::new(0, 1),
            Position::new(1, 1),
            Position::new(2, 1),
        ],
        size: 3,
    };
    let l = Piece {
        blocks: vec![
            Position::new(0, 2),
            Position::new(0, 1),
            Position::new(1, 1),
            Position::new(2, 1),
        ],
        size: 3,
    };
    let o = Piece {
        blocks: vec![
            Position::new(0, 0),
            Position::new(0, 1),
            Position::new(1, 0),
            Position::new(1, 1),
        ],
        size: 2,
    };
    let s = Piece {
        blocks: vec![
            Position::new(1, 0),
            Position::new(2, 0),
            Position::new(0, 1),
            Position::new(1, 1),
        ],
        size: 3,
    };
    let t = Piece {
        blocks: vec![
            Position::new(1, 0),
            Position::new(0, 1),
            Position::new(1, 1),
            Position::new(2, 1),
        ],
        size: 3,
    };
    let z = Piece {
        blocks: vec![
            Position::new(0, 0),
            Position::new(1, 0),
            Position::new(1, 1),
            Position::new(2, 1),
        ],
        size: 3,
    };
    let pieces = vec![
        (i, materials.middle_blue.clone()),
        (j, materials.blurple.clone()),
        (l, materials.quince_jelly.clone()),
        (o, materials.sun_flower.clone()),
        (s, materials.light_greenish_blue.clone()),
        (t, materials.pico_8_pink.clone()),
        (z, materials.chi_gong.clone()),
    ];
    commands.insert_resource(pieces);
}

pub(crate) fn piece_event_handler(
    commands: &mut Commands,
    mut materials: ResMut<Assets<ColorMaterial>>,
    pieces: Res<Vec<(Piece, Handle<ColorMaterial>)>>,
    block_size: Res<BlockSize>,
    mut field: ResMut<Field>,
    mut event_reader: Local<EventReader<PieceEvent>>,
    mut piece_events: ResMut<Events<PieceEvent>>,
    mut field_events: ResMut<Events<FieldEvent>>,
    piece_query: Query<(Entity, &Children, &PiecePosition)>,
    block_query: Query<(&Block, &Handle<ColorMaterial>)>,
    ghost_piece_query: Query<Entity, With<GhostPiece>>,
) {
    let mut spawn_new_piece = false;

    for event in event_reader.iter(&piece_events) {
        match &event {
            PieceEvent::Spawn => {
                spawn_piece(commands, &mut materials, &pieces, block_size.0)
            }
            PieceEvent::Lock => {
                let (piece_entity, blocks, piece_pos) =
                    piece_query.iter().next().unwrap();
                for &b in blocks.iter() {
                    let (block, mat) = block_query.get(b).unwrap();
                    let pos = block.position + piece_pos.0;
                    let Position { x, y } = pos;
                    field.0[y as usize] |= 1 << (FIELD_WIDTH - 1 - x);
                    Block::spawn(commands, mat.clone(), block_size.0, pos);
                }
                commands.despawn_recursive(piece_entity);
                spawn_new_piece = true;

                let ghost_piece_entity =
                    ghost_piece_query.iter().next().unwrap();

                commands.despawn_recursive(ghost_piece_entity);

                for y in 0..FIELD_HEIGHT {
                    if field.0[y as usize] == (1 << FIELD_WIDTH) - 1 {
                        field_events.send(FieldEvent::ClearRow(y));
                    }
                }
            }
        }
    }

    if spawn_new_piece {
        piece_events.send(PieceEvent::Spawn);
    }
}

pub(crate) fn spawn_piece<'a>(
    commands: &'a mut Commands,
    materials: &mut ResMut<Assets<ColorMaterial>>,
    pieces: &[(Piece, Handle<ColorMaterial>)],
    block_size: f32,
) {
    let mut rng = rand::thread_rng();
    let (piece, material) = &pieces[rng.gen_range(0..pieces.len())];
    let mut ghost_color = materials.get(material).unwrap().color;
    ghost_color.set_a(0.1);

    commands
        .spawn((
            ActivePiece {
                piece_size: piece.size,
            },
            Transform::default(),
            GlobalTransform::default(),
            PiecePosition(Position {
                x: ((crate::field::FIELD_WIDTH - piece.size) / 2) as i32,

                // TODO: Change this to be 2 lines above visible
                // field.
                y: 0,
            }),
        ))
        .with_children(|parent| {
            for &block_pos in piece.blocks.iter() {
                Block::spawn_with_parent(
                    parent,
                    material.clone(),
                    block_size,
                    block_pos,
                );
            }
        })
        .spawn((GhostPiece, Transform::default(), GlobalTransform::default()))
        .with_children(|parent| {
            for &block_pos in piece.blocks.iter() {
                Block::spawn_with_parent(
                    parent,
                    materials.add(ghost_color.into()),
                    block_size,
                    block_pos,
                )
                .with(GhostBlock);
            }
        });
}
