#[derive(Debug)]
pub(crate) struct Level(pub u32);

impl Level {
    pub(crate) fn delta_time(&self) -> f32 {
        (0.8 - (self.0 - 1) as f32 * 0.007).powi(self.0 as i32 - 1)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn delta_time_test() {
        assert_eq!(Level(1).delta_time(), 1.0);
        assert_eq!(Level(2).delta_time(), 0.793);
    }
}
