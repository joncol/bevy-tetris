use bevy::prelude::*;

use crate::{
    field::{Field, FIELD_HEIGHT, FIELD_WIDTH},
    movement::is_collision,
    pieces::{GhostBlock, PiecePosition},
    position::Position,
};

pub(crate) const BLOCK_INSET: i32 = 1;

#[derive(Clone, Copy, Debug)]
pub(crate) struct Block {
    pub position: Position,
}

impl Block {
    pub(crate) fn spawn(
        commands: &mut Commands,
        material: Handle<ColorMaterial>,
        size: f32,
        position: Position,
    ) -> &mut Commands {
        commands
            .spawn(SpriteBundle {
                sprite: Sprite::new(Vec2::new(
                    size - BLOCK_INSET as f32,
                    size - BLOCK_INSET as f32,
                )),
                material,
                ..Default::default()
            })
            .with(Block { position })
    }

    pub(crate) fn spawn_with_parent<'a, 'b>(
        parent: &'a mut ChildBuilder<'b>,
        material: Handle<ColorMaterial>,
        size: f32,
        position: Position,
    ) -> &'a mut ChildBuilder<'b> {
        parent
            .spawn(SpriteBundle {
                sprite: Sprite::new(Vec2::new(
                    size - BLOCK_INSET as f32,
                    size - BLOCK_INSET as f32,
                )),
                material,
                ..Default::default()
            })
            .with(Block { position })
    }

    /// Formulas for rotation taken from
    /// https://tech.migge.io/2017/02/07/tetris-rotations
    pub(crate) fn rotate(&mut self, piece_size: i32) {
        let Position { x: old_x, y: old_y } = self.position;
        self.position.x = 1 - (old_y - (piece_size - 2));
        self.position.y = old_x;
    }
}

#[derive(Debug)]
pub(crate) struct BlockSize(pub f32);

pub(crate) fn init_block_size(
    windows: Res<Windows>,
    mut block_size: ResMut<BlockSize>,
) {
    let window = windows.get_primary().unwrap();
    block_size.0 = window.height() as f32 / 30.;
}

/// Transforms block coordinates in world coordinates to screen coordinates.
///
/// The world coordinate system has its origo at the upper left of the playing
/// field, the positive x-axis goes right, and the positive y-axis goes down.
///
/// The screen coordinate system has its origo at the center of the screen, the
/// positive x-axis goes right, and the positive y-axis goes up.
pub(crate) fn block_transform_system(
    block_size: Res<BlockSize>,
    mut query: Query<(Entity, &Block, &mut Transform), Without<GhostBlock>>,
    parent_query: Query<&Parent>,
    piece_pos_query: Query<&PiecePosition>,
) {
    for (entity, block, mut transform) in query.iter_mut() {
        let mut pos: Position = block.position;
        if let Ok(Parent(parent)) = parent_query.get(entity) {
            if let Ok(piece_pos) = piece_pos_query.get(*parent) {
                pos += piece_pos.0;
            }
        }
        transform.translation = Vec3::new(
            (pos.x as f32 - FIELD_WIDTH as f32 / 2.) * block_size.0,
            -(pos.y as f32 - FIELD_HEIGHT as f32 / 2.) * block_size.0,
            0.0,
        );
    }
}

pub(crate) fn ghost_block_transform_system(
    block_size: Res<BlockSize>,
    mut query: Query<(&Block, &mut Transform), With<GhostBlock>>,
    piece_pos_query: Query<&PiecePosition>,
    field: Res<Field>,
) {
    if let Some(piece_pos) = piece_pos_query.iter().next() {
        let mut piece_pos = *piece_pos;

        let block_positions = query
            .iter_mut()
            .map(|(Block { position }, _)| position)
            .collect::<Vec<_>>();

        while !block_positions
            .iter()
            .any(|&&pos| is_collision(piece_pos, pos, &field.0))
        {
            piece_pos.0.y += 1;
        }
        piece_pos.0.y -= 1;

        for (&Block { position }, mut transform) in query.iter_mut() {
            let mut pos: Position = position;
            pos += piece_pos.0;
            transform.translation = Vec3::new(
                (pos.x as f32 - FIELD_WIDTH as f32 / 2.) * block_size.0,
                -(pos.y as f32 - FIELD_HEIGHT as f32 / 2.) * block_size.0,
                0.0,
            );
        }
    }
}
