use std::time::Duration;

use bevy::{
    diagnostic::{Diagnostics, FrameTimeDiagnosticsPlugin},
    ecs::RunOnce,
    prelude::*,
};

mod block;
mod border;
mod field;
mod input;
mod level;
mod lock_timer;
mod movement;
mod pieces;
mod position;

#[derive(Debug)]
struct Score(u32);

#[derive(Debug)]
struct ClearedLinesCount(u32);

struct FpsText;

struct ScoreText;

struct LevelText;

#[derive(Debug)]
struct DropTimer(Timer);

struct Pause(bool);

#[derive(Debug)]
struct GameOver(bool);

fn main() {
    let mut app = App::build();
    app.add_plugins(DefaultPlugins);

    // #[cfg(target_arch = "wasm32")]
    // app.add_plugin(bevy_webgl2::WebGL2Plugin);

    let mut lock_timer =
        Timer::from_seconds(lock_timer::LOCK_DELAY.as_secs_f32(), false);
    lock_timer.pause();

    app.add_plugin(FrameTimeDiagnosticsPlugin::default())
        .init_resource::<input::gamepad::GamepadLobby>()
        .add_resource(ClearColor(Color::BLACK))
        .add_resource(block::BlockSize(0.0))
        .add_resource(ClearedLinesCount(0))
        .add_resource(Score(0))
        .add_resource(level::Level(1))
        .add_resource(Pause(false))
        .add_resource(GameOver(false))
        .add_resource(input::keyboard::KeyboardRepeatTimer(
            Timer::from_seconds(input::MOVE_REPEAT_INTERVAL, true),
        ))
        .add_resource(input::gamepad::GamepadRepeatTimer(Timer::from_seconds(
            input::MOVE_REPEAT_INTERVAL,
            true,
        )))
        .add_resource(lock_timer::LockTimer(lock_timer))
        .add_resource(field::Field(vec![0; field::FIELD_HEIGHT as usize]))
        .add_resource(DropTimer(Timer::new(Duration::from_millis(1000), true)))
        .add_startup_system(setup.system())
        .add_startup_stage(
            "game_setup",
            SystemStage::serial().with_system(pieces::create_pieces.system()),
        )
        // This needs to run after the window size has been properly
        // initialized, since the block size calculation depends on that.
        .add_stage_after(
            stage::LAST,
            "game_start",
            SystemStage::serial()
                .with_run_criteria(RunOnce::default())
                .with_system(block::init_block_size.system())
                .with_system(init_game.system())
                .with_system(border::setup_border.system()),
        )
        .add_system_to_stage(
            stage::PRE_UPDATE,
            input::gamepad::connection_system.system(),
        )
        .add_system(game_tick.system())
        .add_system(input::keyboard::keyboard_system.system())
        .add_system(input::gamepad::gamepad_system.system())
        .add_system(input::gamepad::gamepad_events_system.system())
        .add_system(movement::movement_system.system())
        .add_system(score_text.system())
        .add_system(level_text.system())
        .add_system(fps_text.system())
        .add_system(field::field_event_handler.system())
        .add_system(pieces::piece_event_handler.system())
        .add_system(lock_timer::lock_timer_handler.system())
        .add_stage_after(
            stage::UPDATE,
            "transform",
            SystemStage::serial()
                .with_system(block::block_transform_system.system())
                .with_system(block::ghost_block_transform_system.system()),
        )
        .add_event::<field::FieldEvent>()
        .add_event::<movement::MoveEvent>()
        .add_event::<lock_timer::LockTimerEvent>()
        .add_event::<pieces::PieceEvent>()
        .run();
}

fn setup(commands: &mut Commands, asset_server: Res<AssetServer>) {
    commands
        .spawn(CameraUiBundle::default())
        .spawn(Camera2dBundle::default())
        .spawn(TextBundle {
            text: Text {
                font: asset_server.load("fonts/FiraMono-Medium.ttf"),
                value: "".to_string(),
                style: TextStyle {
                    color: Color::rgb(0.9, 0.9, 0.9),
                    font_size: 20.0,
                    ..Default::default()
                },
            },
            style: Style {
                position_type: PositionType::Absolute,
                position: Rect {
                    top: Val::Px(5.0),
                    left: Val::Px(5.0),
                    ..Default::default()
                },
                ..Default::default()
            },
            ..Default::default()
        })
        .with(ScoreText)
        .spawn(TextBundle {
            text: Text {
                value: "FPS".to_string(),
                font: asset_server.load("fonts/FiraMono-Medium.ttf"),
                style: TextStyle {
                    font_size: 15.0,
                    color: Color::hex("f1c40f").unwrap(),
                    ..Default::default()
                },
            },
            style: Style {
                align_self: AlignSelf::FlexStart,
                ..Default::default()
            },
            ..Default::default()
        })
        .with(FpsText)
        .spawn(TextBundle {
            text: Text {
                value: "Level: ".to_string(),
                font: asset_server.load("fonts/FiraMono-Medium.ttf"),
                style: TextStyle {
                    font_size: 20.0,
                    color: Color::hex("f8efba").unwrap(),
                    ..Default::default()
                },
            },
            style: Style {
                position_type: PositionType::Absolute,
                position: Rect {
                    top: Val::Px(25.0),
                    left: Val::Px(5.0),
                    ..Default::default()
                },
                ..Default::default()
            },
            ..Default::default()
        })
        .with(LevelText);
}

fn score_text(score: Res<Score>, mut query: Query<(&ScoreText, &mut Text)>) {
    let (_score_board, mut text) = query.iter_mut().next().unwrap();
    text.value = format!("Score: {}", score.0);
}

fn level_text(
    level: Res<level::Level>,
    mut query: Query<(&LevelText, &mut Text)>,
) {
    let (_score_board, mut text) = query.iter_mut().next().unwrap();
    text.value = format!("Level: {}", level.0);
}

fn fps_text(
    diagnostics: Res<Diagnostics>,
    mut query: Query<(&FpsText, &mut Text)>,
) {
    for (_tag, mut text) in query.iter_mut() {
        if let Some(fps) = diagnostics.get(FrameTimeDiagnosticsPlugin::FPS) {
            if let Some(average) = fps.average() {
                text.value = format!("FPS: {:.2}", average);
            }
        }
    }
}

fn init_game(mut piece_events: ResMut<Events<pieces::PieceEvent>>) {
    piece_events.send(pieces::PieceEvent::Spawn);
}

fn game_tick(
    time: Res<Time>,
    mut drop_timer: ResMut<DropTimer>,
    mut move_events: ResMut<Events<movement::MoveEvent>>,
    pause: Res<Pause>,
    game_over: Res<GameOver>,
) {
    if pause.0 || game_over.0 {
        return;
    }

    if drop_timer.0.tick(time.delta_seconds()).finished() {
        move_events.send(movement::MoveEvent::Move(movement::Direction::Down));
    }
}
