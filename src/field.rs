use bevy::prelude::*;

use crate::{
    block::Block, border::Border, level::Level, ClearedLinesCount, Score,
};

pub(crate) const FIELD_WIDTH: i32 = 10;
pub(crate) const FIELD_HEIGHT: i32 = 20;

#[derive(Debug)]
pub(crate) struct Field(pub Vec<u32>);

#[derive(Debug)]
pub(crate) enum FieldEvent {
    ClearRow(i32),
}

#[derive(Debug, Default)]
pub(crate) struct LastWasTetris(pub bool);

pub(crate) fn field_event_handler(
    commands: &mut Commands,
    mut events_reader: Local<EventReader<FieldEvent>>,
    field_events: Res<Events<FieldEvent>>,
    mut field: ResMut<Field>,
    mut block_query: Query<(Entity, &mut Block), Without<Border>>,
    mut level: ResMut<Level>,
    mut cleared_lines_count: ResMut<ClearedLinesCount>,
    mut score: ResMut<Score>,
    mut last_was_tetris: Local<LastWasTetris>,
) {
    let mut cleared_count = 0;
    for event in events_reader.iter(&field_events) {
        match event {
            FieldEvent::ClearRow(row) => {
                cleared_count += 1;
                for (block_entity, mut block) in block_query.iter_mut() {
                    if block.position.y == *row {
                        commands.despawn(block_entity);
                    } else if block.position.y < *row {
                        block.position.y += 1;
                    }
                }

                field.0[0] = 0;
                for y in (0..=row - 1).rev() {
                    field.0[y as usize + 1] = field.0[y as usize];
                }
            }
        }
    }
    cleared_lines_count.0 += cleared_count;
    score.0 += match cleared_count {
        0 => 0,
        1 => 100,
        2 => 300,
        3 => 500,
        4 => {
            let score = if last_was_tetris.0 { 1200 } else { 800 };
            score
        }
        _ => unreachable!(),
    } * level.0;

    if cleared_count > 0 {
        last_was_tetris.0 = cleared_count == 4;
    }

    if cleared_lines_count.0 >= 5 * level.0 {
        cleared_lines_count.0 -= 5 * level.0;
        level.0 += 1;
    }
}
