use std::time::Duration;

use bevy::prelude::*;

use crate::pieces::PieceEvent;

pub(crate) const LOCK_DELAY: Duration = Duration::from_millis(500);

#[derive(Debug)]
pub(crate) struct LockTimer(pub Timer);

#[derive(Debug)]
pub(crate) enum LockTimerEvent {
    Start,
    Stop,
}

pub(crate) fn lock_timer_handler(
    time: Res<Time>,
    events: ResMut<Events<LockTimerEvent>>,
    mut events_reader: Local<EventReader<LockTimerEvent>>,
    mut piece_events: ResMut<Events<PieceEvent>>,
    mut lock_timer: ResMut<LockTimer>,
) {
    if !lock_timer.0.paused() {
        lock_timer.0.tick(time.delta_seconds());
        if lock_timer.0.finished() {
            lock_timer.0.pause();
            piece_events.send(PieceEvent::Lock);
        }
    }
    for event in events_reader.iter(&events) {
        match *event {
            LockTimerEvent::Start => {
                lock_timer.0.reset();
                lock_timer.0.unpause();
            }
            LockTimerEvent::Stop => {
                lock_timer.0.pause();
            }
        }
    }
}
