use bevy::prelude::*;

use crate::movement::{Direction, MoveEvent};

use super::{MOVE_REPEAT_DELAY, MOVE_REPEAT_INTERVAL};

#[derive(Debug)]
pub(crate) struct KeyboardRepeatTimer(pub Timer);

pub(crate) fn keyboard_system(
    keyboard_input: Res<Input<KeyCode>>,
    mut move_events: ResMut<Events<MoveEvent>>,
    time: Res<Time>,
    mut repeat_timer: ResMut<KeyboardRepeatTimer>,
) {
    if keyboard_input.just_pressed(KeyCode::H)
        || keyboard_input.just_pressed(KeyCode::Left)
    {
        repeat_timer.0.set_duration(MOVE_REPEAT_DELAY);
        &repeat_timer.0.reset();
        move_events.send(MoveEvent::Move(Direction::Left));
    }
    if keyboard_input.just_pressed(KeyCode::L)
        || keyboard_input.just_pressed(KeyCode::Right)
    {
        repeat_timer.0.set_duration(MOVE_REPEAT_DELAY);
        &repeat_timer.0.reset();
        move_events.send(MoveEvent::Move(Direction::Right));
    }

    if !repeat_timer.0.finished() {
        repeat_timer.0.tick(time.delta_seconds());
    } else {
        repeat_timer.0.set_duration(MOVE_REPEAT_INTERVAL);
        &repeat_timer.0.reset();

        if keyboard_input.pressed(KeyCode::H)
            || keyboard_input.pressed(KeyCode::Left)
        {
            move_events.send(MoveEvent::Move(Direction::Left));
        }
        if keyboard_input.pressed(KeyCode::L)
            || keyboard_input.pressed(KeyCode::Right)
        {
            move_events.send(MoveEvent::Move(Direction::Right));
        }
    }

    if keyboard_input.just_pressed(KeyCode::K)
        || keyboard_input.just_pressed(KeyCode::Up)
    {
        move_events.send(MoveEvent::Rotate);
    }

    if keyboard_input.just_pressed(KeyCode::J)
        || keyboard_input.just_pressed(KeyCode::Down)
    {
        move_events.send(MoveEvent::StartSoftDrop);
    } else if keyboard_input.just_released(KeyCode::J)
        || keyboard_input.just_released(KeyCode::Down)
    {
        move_events.send(MoveEvent::StopSoftDrop);
    }
}
