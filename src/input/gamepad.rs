use bevy::prelude::*;

use std::collections::HashSet;

use crate::movement::{Direction, MoveEvent};

use super::{MOVE_REPEAT_DELAY, MOVE_REPEAT_INTERVAL};

#[derive(Default)]
pub(crate) struct GamepadLobby {
    gamepads: HashSet<Gamepad>,
    gamepad_event_reader: EventReader<GamepadEvent>,
}

pub(crate) struct GamepadRepeatTimer(pub Timer);

pub(crate) fn connection_system(
    mut lobby: ResMut<GamepadLobby>,
    gamepad_event: Res<Events<GamepadEvent>>,
) {
    for event in lobby.gamepad_event_reader.iter(&gamepad_event) {
        match &event {
            GamepadEvent(gamepad, GamepadEventType::Connected) => {
                lobby.gamepads.insert(*gamepad);
            }
            GamepadEvent(gamepad, GamepadEventType::Disconnected) => {
                lobby.gamepads.remove(gamepad);
            }
            _ => (),
        }
    }
}

pub(crate) fn gamepad_system(
    lobby: Res<GamepadLobby>,
    // button_inputs: Res<Input<GamepadButton>>,
    // button_axes: Res<Axis<GamepadButton>>,
    axes: Res<Axis<GamepadAxis>>,
    mut move_events: ResMut<Events<MoveEvent>>,
    time: Res<Time>,
    mut repeat_timer: ResMut<GamepadRepeatTimer>,
) {
    if !repeat_timer.0.finished() {
        repeat_timer.0.tick(time.delta_seconds());
    } else {
        repeat_timer.0.set_duration(MOVE_REPEAT_INTERVAL);
        &repeat_timer.0.reset();

        for gamepad in lobby.gamepads.iter().cloned() {
            let dpadx = axes
                .get(GamepadAxis(gamepad, GamepadAxisType::DPadX))
                .unwrap();
            if dpadx < -0.01 {
                move_events.send(MoveEvent::Move(Direction::Left));
            }
            if dpadx > 0.01 {
                move_events.send(MoveEvent::Move(Direction::Right));
            }
        }
    }
}

pub(crate) fn gamepad_events_system(
    mut event_reader: Local<EventReader<GamepadEvent>>,
    gamepad_event: Res<Events<GamepadEvent>>,
    mut move_events: ResMut<Events<MoveEvent>>,
    mut repeat_timer: ResMut<GamepadRepeatTimer>,
) {
    for event in event_reader.iter(&gamepad_event) {
        match &event {
            GamepadEvent(
                _gamepad,
                GamepadEventType::ButtonChanged(button_type, value),
            ) => {
                if *button_type == GamepadButtonType::East && *value > 0.1 {
                    move_events.send(MoveEvent::Rotate);
                } else if *button_type == GamepadButtonType::South
                    && *value > 0.1
                {
                    move_events.send(MoveEvent::StartSoftDrop);
                }
            }
            GamepadEvent(
                _gamepad,
                GamepadEventType::AxisChanged(axis_type, value),
            ) => {
                if *axis_type == GamepadAxisType::DPadX && value.abs() > 0.1 {
                    repeat_timer.0.set_duration(MOVE_REPEAT_DELAY);
                    &repeat_timer.0.reset();
                    if *value < 0.0 {
                        move_events.send(MoveEvent::Move(Direction::Left));
                    } else {
                        move_events.send(MoveEvent::Move(Direction::Right));
                    }
                } else if *axis_type == GamepadAxisType::DPadY
                    && value.abs() > 0.1
                {
                    if *value < 0.0 {
                        move_events.send(MoveEvent::StartSoftDrop);
                    } else {
                        move_events.send(MoveEvent::Rotate);
                    }
                }
            }
            _ => {}
        }
    }
}
