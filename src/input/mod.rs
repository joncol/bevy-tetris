pub mod gamepad;
pub mod keyboard;

pub(crate) const MOVE_REPEAT_DELAY: f32 = 0.25;
pub(crate) const MOVE_REPEAT_INTERVAL: f32 = 0.01;
