use bevy::prelude::*;

use crate::{
    block::Block,
    field::{Field, FIELD_HEIGHT, FIELD_WIDTH},
    level::Level,
    lock_timer::{LockTimer, LockTimerEvent},
    pieces::{ActivePiece, GhostPiece, PieceEvent, PiecePosition},
    position::Position,
    DropTimer, GameOver, Pause,
};

#[derive(Debug)]
pub(crate) enum MoveEvent {
    Move(Direction),
    Rotate,
    StartSoftDrop,
    StopSoftDrop,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub(crate) enum Direction {
    Left,
    Right,
    Down,
}

pub(crate) fn movement_system(
    move_events: Res<Events<MoveEvent>>,
    mut piece_events: ResMut<Events<PieceEvent>>,
    mut lock_timer_events: ResMut<Events<LockTimerEvent>>,
    mut move_events_reader: Local<EventReader<MoveEvent>>,
    field: Res<Field>,
    pause: Res<Pause>,
    mut lock_timer: ResMut<LockTimer>,
    mut drop_timer: ResMut<DropTimer>,
    level: Res<Level>,
    mut game_over: ResMut<GameOver>,
    mut piece_query: Query<(&Children, &ActivePiece, &mut PiecePosition)>,
    mut ghost_piece_query: Query<(&Children, &GhostPiece)>,
    mut block_query: Query<&mut Block>,
) {
    if pause.0 || game_over.0 {
        return;
    }

    for e in move_events_reader.iter(&move_events) {
        if let Some((blocks, active_piece, mut piece_pos)) =
            piece_query.iter_mut().next()
        {
            match e {
                MoveEvent::Move(dir) => {
                    let mut delta = Position::default();
                    match dir {
                        Direction::Left => {
                            delta.x -= 1;
                            lock_timer.0.reset();
                        }
                        Direction::Right => {
                            delta.x += 1;
                            lock_timer.0.reset();
                        }
                        Direction::Down => delta.y += 1,
                    }

                    // Check if move would cause collision.
                    let mut collision = false;
                    for &b in blocks.iter() {
                        let block = block_query.get_mut(b).unwrap();
                        if is_collision(
                            PiecePosition(piece_pos.0 + delta),
                            block.position,
                            &field.0,
                        ) {
                            collision = true;
                            break;
                        }
                    }

                    if collision && *dir == Direction::Down {
                        if piece_pos.0.y <= 0 {
                            game_over.0 = true;
                        }
                        if lock_timer.0.paused() {
                            lock_timer_events.send(LockTimerEvent::Start);
                        } else if lock_timer.0.finished() {
                            piece_events.send(PieceEvent::Lock);
                        }
                    } else if !collision {
                        lock_timer_events.send(LockTimerEvent::Stop);
                        piece_pos.0 += delta;
                    }
                }
                MoveEvent::Rotate => {
                    let mut collision = false;

                    let mut rot_blocks = Vec::new();
                    for &b in blocks.iter() {
                        let block = block_query.get_mut(b).unwrap();
                        let mut block_copy = block.clone();
                        block_copy.rotate(active_piece.piece_size);

                        if is_collision(
                            PiecePosition(piece_pos.0),
                            block_copy.position,
                            &field.0,
                        ) {
                            collision = true;
                            break;
                        }

                        rot_blocks.push(block_copy);
                    }

                    if !collision {
                        for (&b, &rot_block) in
                            blocks.iter().zip(rot_blocks.iter())
                        {
                            let _ = block_query.set(b, rot_block).unwrap();
                        }
                        lock_timer.0.reset();

                        if let Some((blocks, _)) =
                            ghost_piece_query.iter_mut().next()
                        {
                            for (&b, &rot_block) in
                                blocks.iter().zip(rot_blocks.iter())
                            {
                                let _ = block_query.set(b, rot_block).unwrap();
                            }
                        }
                    }
                }
                MoveEvent::StartSoftDrop => drop_timer.0.set_duration(0.05),
                MoveEvent::StopSoftDrop => {
                    drop_timer.0.set_duration(level.delta_time())
                }
            }
        }
    }
}

pub(crate) fn is_collision(
    piece_pos: PiecePosition,
    block_pos: Position,
    field: &[u32],
) -> bool {
    let pos = piece_pos.0 + block_pos;
    pos.x < 0
        || pos.x >= FIELD_WIDTH
        || pos.y >= FIELD_HEIGHT
        || field[pos.y as usize] & (1 << (FIELD_WIDTH - 1 - pos.x)) != 0
}
